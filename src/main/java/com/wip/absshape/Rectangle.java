/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wip.absshape;

/**
 *
 * @author WIP
 */
public class Rectangle extends Shape {
    double w, h;

    public Rectangle(double w, double h) {
        super("Rectangle");
        this.w = w;
        this.h = h;
    }

    @Override
    public double calArea() {
        return w * h;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "w=" + w + ", h=" + h + '}';
    }
    
    
    
}
